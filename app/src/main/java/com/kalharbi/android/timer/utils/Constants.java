package com.kalharbi.android.timer.utils;

/**
 * Created by Khalid Alharbi
 */
public class Constants {
    public static final int START_COUNTER = 100;
    public static final int STOP_COUNTER = 200;
    public static final int STATUS_RESPONSE = 300;
    public static final int PROGRESS_RESPONSE = 400;
    public static final String PROGRESS_KEY = "PROGRESS";
    public static final String START_VALUE_KEY = "START_VALUE";
    public static final String STATUS_KEY = "STATUS";

}
