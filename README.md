# Android Timer
### Description
This is an example on how to create a basic timer in Android using background services.
The service in this example is a bound service that communicates with the activity in a client-server interface. That's it, the service acts as a server and the activity is the client that binds to the service, sends requests, and receives response. The service continues running in the background even if the activity is destroyed and displays a notification in the notification area.

### Screenshot
![Android Timer](https://bytebucket.org/kalharbi/timer/raw/88103c8551c5b3a72b9f314ad597e718f4c1e8e7/.screenshots/timer_screenshot.png "Android Timer")

##Problems and Bugs Examples:
## Bug-01 Branch
### Description:
Android defines a service as "an application component that can perform long-running operations in the background and does not provide a user interface." But that does not mean it always runs in a background thread!

This branch has a bug that causes the app to crash in "Application Not Responding" (ANR) error dialog. The reason is that the background service (*/services/TimerService.java*) runs in the main thread or "UI thread" and it blocks the main thread while waiting for the timer to complete, and thus, preventing the system from processing incoming user input events.
A service runs in the main thread of its hosting process by default. Android does not create a background thread for services by default, nor does the service create its own thread or runs in a separate process.
### How to fix this bug?
We should create a new thread within the service to do all the service's work even if it's not intensive or a small blocking operation. This can be done in one of two ways:

**1. Extend the _IntentService_ class instead of the *Service* class:**

This class creates a default worker thread that executes all intents delivered to onStartCommand separate from the app's main thread. For Bound services, we need to implement the onBind() method since the default implementation returns null, so we should return an IBinder object that defines the interface for communication with the service. The service's work is executed in a method called *onHandleIntent()*, so we need to implement this method to do the service's work in the created worker thread.
However, this class has a limitation in which the service can not execute multiple requests simultaneously. That's it, the service always waits for the previous request to finish. This is not what this app does, but I'll implement the second solution because it's more generic and can handle many requests simultaneously. 

**2. Extend the _Service_ class:**

We must create a new thread that executes the background task because the service normally runs in the process's main thread which blocks UI events. We should also make the thread a background priority thread to avoid disrupting the UI. This thread should be created in the *onCreate()* method since Android calls this method when the service is not already running.

The second solution is implemented in */services/TimerService.java* in branch fix-01.
### References:
-  [Android API Guides - Services](http://developer.android.com/guide/components/services.html)
- [Stackoverflow](http://stackoverflow.com/a/10530226)
- [Antipattern: freezing the UI with a Service and an IntentService](http://gmariotti.blogspot.com/2013/03/antipattern-freezing-ui-with-service.html)

## Bug-02 Branch
### Description:
Service Binding Problem: Binding to a service is asynchronous which means the service will not start right after it’s bound. That's it, ServiceConnection.onServiceConnected() is not called immediately. Therefore, it's very important to delay posting any request to send or receive messages from the service. This problem causes the app to crash in NullPointerException because the messenger object is not yet initialized. It's a very difficult bug to find and has many other possible ways to surface. 
This bug is in branch bug-02 and can be reproduced by starting the counter and changing the current device orientation.

### How to fix this bug?
Simply, do not start sending messages to the service right after binding to it. Instead, delay the request in a different thread until the service is connected. This can be done by using a Handler object to postDelay the request, so it runs later in a new Runnable thread. The fix is in branch fix-02.

### References:
*  [Stackoverflow](http://stackoverflow.com/a/10807311)
*  Satya Komatineni et al. Pro Android 4. New York: Apress, 2012. 425-426
* Google this: service onserviceconnected never called, and you will find many questions ans different answers to fix this problem.

## Bug-03 Branch
## Description
Leaking a Context in an Inner Class: A Non-static inner class always keeps a reference to its enclosing object, so it can refer to the instance variables of the enclosing object. In the class activities/MainActivity2.java, we have a non-static inner class that retains a reference to its enclosing class (MainActivity2). This is actually not a problem as long as this object does not live longer than the Activity object. But declaring it as a static variable means it will live longer than any instance object. In addition to retaining a reference to the activity, the AsyncTask is not canceled when the Activity dies wastes CPU resources and makes the task retains a reference to the activity until it finishes.

### How to fix this bug?
Avoid using non-static inner classes in an activity if an instance of the inner class retains a reference to the activity (context) and will live longer than the activity object. To fix this problem, cancel the task in _onDestroy()_ and always use a static inner class with a WeakReference to the outer class, as done in branch fix-03. 

### References:

*  [Memory Analysis for Android Applications](http://android-developers.blogspot.com/2011/03/memory-analysis-for-android.html) 
The complete talk about this blog post is here: [Google I/O 2011: Memory management for Android Apps](http://youtu.be/_CruQY55HOk)
*  [Avoiding memory leaks](http://android-developers.blogspot.com/2009/01/avoiding-memory-leaks.html)
*  [Activitys, Threads, & Memory Leaks](http://www.androiddesignpatterns.com/2013/04/activitys-threads-memory-leaks.html)
*  [Android's AsyncTask](http://steveliles.github.io/android_s_asynctask.html)
