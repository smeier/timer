package com.kalharbi.android.timer.services;


import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.Process;
import android.os.RemoteException;
import android.util.Log;

import com.kalharbi.android.timer.core.CountDownTimer;
import com.kalharbi.android.timer.utils.Constants;

/**
 * @author Khalid Alharbi
 *         <p/>
 *         A bound background service that starts a timer and sneds messages to the bounding client.
 */
public class TimerService extends Service {
    private static final String TAG = TimerService.class.getName();
    /**
     * Messenger Interface that refers to a Handler, which we can use to receive messages.
     */
    private Messenger mMessenger;
    private Looper mServiceLooper;
    private IncomingHandler mServiceHandler;
    private CountDownTimer myCountDownTimer;

    // This is a bound service, so set a flag to true to indicate that onRebind is used.
    private boolean mAllowRebind = true;

    /**
     * Called when the service is being created. We must create a
     * separate thread because the service normally runs in the process's
     * main thread, which we don't want to block.
     */
    @Override
    public void onCreate() {
        // Start up the thread running the service.  Note that we   We also make it
        // background priority so CPU-intensive work will not disrupt our UI.
        HandlerThread thread = new HandlerThread("ServiceStartArguments",
                Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();
        Log.d(TAG, "the service has been created.");
        // Get the HandlerThread's Looper and use it for our Handler
        mServiceLooper = thread.getLooper();
        mServiceHandler = new IncomingHandler(mServiceLooper);
        mMessenger = new Messenger(mServiceHandler);
    }

    /**
     * A client is binding to the service with bindService() Called when a
     * client is binding to the service. We return the communication channel to
     * the service for sending messages to the service.
     */
    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "A client has bound to the service.");
        // return the communication channel.
        return mMessenger.getBinder();
    }

    /**
     * Called when all clients have unbound with unbindService()
     */
    @Override
    public boolean onUnbind(Intent intent) {
        Log.d(TAG, "All clients have unbound from the service.");
        if (myCountDownTimer != null) {
            myCountDownTimer.stop();
        }
        return mAllowRebind;
    }

    /**
     * A client is binding to the service with bindService() again, after
     * onUnbind() has already been called
     */
    @Override
    public void onRebind(Intent intent) {
        Log.d(TAG, "A client has rebound to the service.");
    }

    /**
     * The system calls this method when the service is no longer used and is
     * being destroyed. This is the last call the service receive.
     */
    @Override
    public void onDestroy() {
        Log.d(TAG, "The service has been destroyed.");
    }

    /**
     * An incoming handler that receives incoming messages from clients. This class is
     * static to avoid potential memory leaks. If IncomingHandler class is not
     * static, it will have a reference to the Service object. and the service
     * object cannot be garbage collected, even after being destroyed.
     */
    private final class IncomingHandler extends Handler {
        public IncomingHandler(Looper looper) {
            super(looper);
        }

        // We do the real work here.
        @Override
        public void handleMessage(Message msg) {

            switch (msg.what) {
                case Constants.START_COUNTER:
                    // get incoming data
                    long startValue = msg.getData().getInt(Constants.START_VALUE_KEY);
                    Log.d(TAG, "Starting counter at " + startValue);
                    // Get the Messenger where replies to this message can be sent.
                    Messenger replyMessenger = msg.replyTo;

                    // Create the response message and prepare the response message.
                    Bundle progressReplyBundle = new Bundle();
                    Message progressResponseMsg;

                    // Create a new instance of the CountDown object
                    myCountDownTimer = new CountDownTimer(startValue);
                    myCountDownTimer.start();
                    while (!myCountDownTimer.isFinished()) {
                        try {
                            Thread.sleep(500);
                            progressResponseMsg = Message.obtain(null,
                                    Constants.PROGRESS_RESPONSE);
                            progressReplyBundle.putLong(Constants.PROGRESS_KEY, myCountDownTimer.getStartCounterValueSeconds());
                            progressResponseMsg.setData(progressReplyBundle);
                            replyMessenger.send(progressResponseMsg);
                            //progressReplyBundle.clear();
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        Log.d(TAG, Long.toString(myCountDownTimer.getStartCounterValueSeconds()));
                    }

                    // Create the response message and prepare the response message.
                    Bundle finalReplyBundle = new Bundle();
                    finalReplyBundle.putString(Constants.STATUS_KEY, "DONE");
                    Message finalResponseMsg = Message.obtain(null,
                            Constants.STATUS_RESPONSE);
                    finalResponseMsg.setData(finalReplyBundle);

                    try {
                        // Send a respond Message to the reply Messenger's Handler
                        if (replyMessenger != null) {
                            replyMessenger.send(finalResponseMsg);
                        } else {
                            Log.e(TAG, "Reply Messenger can not be found.");
                        }

                    } catch (RemoteException e) {
                        Log.e(TAG, e.getMessage());
                    }
                    break;
                case Constants.STOP_COUNTER:
                    if (myCountDownTimer != null) {
                        Log.d(TAG, "Stopping service...");
                        myCountDownTimer.stop();
                    }
                    break;
            }

            /* Stop the service using the startId (msg.arg1), if you don't want to stop
            a service in the middle of handling another job. Here, we want to stop everything.
            */
            stopSelf();
        }
    }


}
